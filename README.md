# TASK MANAGER

## SCREENSHOTS

https://disk.yandex.ru/d/_iHvwB5jkMlq7w?w=1

## DEVELOPER INFO

name: Vladimir Korenyugin

e-mail: vkorenygin@tsconsulting.com

e-mail: koryneugene@gmail.com

## SOFTWARE REQUIREMENTS

System: Windows 10 Enterprise LTSC

JDK version: 1.8.0_282

## HARDWARE REQUIREMENTS

CPU: i5-8250U

RAM: 16GB

SSD: 512Gb

## PROGRAM LAUNCH COMMAND

```bash
java –jar ./task-manager.jar
```